package com.eventosapp.models;

public class RegistroLigacoesSearch {

	private String ramal;
	private String dst;
	

	public String getDst() {
		return dst;
	}

	public void setDst(String dst) {
		this.dst = dst;
	}

	public String getRamal() {
		return ramal;
	}

	public void setRamal(String ramal) {
		this.ramal = ramal;
	}
	
	
	
}
