package com.eventosapp.models;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.eventosapp.entity.RegistroLigacoesEntity;

public class RegistroLigacoesSpecification implements Specification<RegistroLigacoesEntity> {

	
	
	private RegistroLigacoesSearch filtro;
	
	public RegistroLigacoesSpecification(RegistroLigacoesSearch filtro) {
		super();
		this.filtro = filtro;
	}
	
	public Predicate toPredicate(Root<RegistroLigacoesEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

		Predicate p = cb.disjunction();

		if (filtro.getRamal() != null) {
			p.getExpressions().add(cb.equal(root.get("src"), filtro.getRamal()));
		}

		return p;

	}
	
	
	
	
	
	
	
	
	/*
	private RegistroLigacoesEntity filtro;

	public RegistroLigacoesSpecification(RegistroLigacoesEntity filtro) {
		super();
		this.filtro = filtro;
	}

	public Predicate toPredicate(Root<RegistroLigacoesEntity> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {

		Predicate p = cb.disjunction();

		if (filtro.getSrc() != null) {
			p.getExpressions().add(cb.equal(root.get("src"), filtro.getSrc()));
		}

		return p;

	}
*/
}
