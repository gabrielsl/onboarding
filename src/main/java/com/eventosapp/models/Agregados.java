package com.eventosapp.models;

public class Agregados {

	
	private final long count;
	private final long sum;
	
	public Agregados(long count, long sum) {
		this.count = count;
		this.sum = sum;
	}
	
	public long getCount() {
		return count;
	}
	public long getSum() {
		return sum;
	}

	
}
