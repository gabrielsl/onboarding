package com.eventosapp.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eventosapp.models.Evento;
import com.eventosapp.repository.EventoRepository;

@RestController
public class ControllerRestEvento {

	@Autowired
	private EventoRepository er;

	// End Points
	@RequestMapping(value = "/cadastro", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Evento> cadastrarEvento(@RequestBody Evento evento) {
		er.save(evento);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/cadastro", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Evento>> listarEvento() {
		return new ResponseEntity<>((Collection<Evento>) er.findAll(), HttpStatus.CREATED);
	}

	//Download de um arquivo
	@RequestMapping(value = "/download/{nomeArquivo}", method = RequestMethod.GET)
	public HttpEntity<byte[]> download(@PathVariable("nomeArquivo") String nomeArquivo) throws IOException {

		System.out.println(nomeArquivo);
		byte[] arquivo = Files.readAllBytes(Paths.get("/home/luizgustavo/Imagens/"+nomeArquivo));

		HttpHeaders httpHeaders = new HttpHeaders();

		httpHeaders.add("Content-Disposition", "attachment;filename=\"download.png\"");

		HttpEntity<byte[]> entity = new HttpEntity<byte[]>(arquivo, httpHeaders);

		return entity;
	}
	
	
}
