package com.eventosapp.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eventosapp.entity.RegistroLigacoesEntity;
import com.eventosapp.models.Agregados;
import com.eventosapp.models.RegistroLigacoesSearch;
import com.eventosapp.models.RegistroLigacoesSpecification;
import com.eventosapp.repository.RegistroLigacoesRepository;

@RestController
@RequestMapping("/ligacoes")
public class RegistroLigacoesController {

	@Autowired
	RegistroLigacoesRepository repository;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Collection<RegistroLigacoesEntity>> buscarLigacoes() {
		return new ResponseEntity<>((Collection<RegistroLigacoesEntity>) repository.findAll(), HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Collection<RegistroLigacoesEntity>> buscarPorId(@PathVariable("id") String uniqueid) {
		return new ResponseEntity<>((Collection<RegistroLigacoesEntity>) repository.findByUniqueid(uniqueid),
				HttpStatus.OK);
	}

	@RequestMapping(value = "/agregados", method = RequestMethod.GET)
	public ResponseEntity<Agregados> buscarAgregados() {
		return new ResponseEntity<>(repository.retorno(), HttpStatus.OK);
	}

	/*
	@RequestMapping(value = "/ramal/{ramal}", method = RequestMethod.GET)
	public ResponseEntity<Collection<RegistroLigacoesEntity>> filtrarLigacoes(
			@PathVariable(value = "ramal", required = false) String ramal) {

		RegistroLigacoesEntity filter = new RegistroLigacoesEntity();
		filter.setSrc(ramal);

		Specification<RegistroLigacoesEntity> spec = new RegistroLigacoesSpecification(filter);

		return new ResponseEntity<>((Collection<RegistroLigacoesEntity>) repository.findAll(spec), HttpStatus.OK);
	}
	*/
	
	
	
	/*
	@RequestMapping(value = "/ramal", method = RequestMethod.GET)
	public ResponseEntity<Collection<RegistroLigacoesEntity>> filtrarLigacoes2(
			@RequestParam(value = "ramal", required = false) String ramal) {

		RegistroLigacoesEntity filter = new RegistroLigacoesEntity();
		filter.setSrc(ramal);

		Specification<RegistroLigacoesEntity> spec = new RegistroLigacoesSpecification(filter);

		return new ResponseEntity<>((Collection<RegistroLigacoesEntity>) repository.findAll(spec), HttpStatus.OK);
	}

	*/
	
	
	@RequestMapping(value = "/ramal", method = RequestMethod.GET)
	public ResponseEntity<Collection<RegistroLigacoesEntity>> filtrarLigacoes2(
			@RequestParam(value = "ramal", required = false) String ramal,
			@RequestParam(value = "dst", required = false) String dst,
			@RequestParam(value = "dataInicio", required = false) String dataInicio) {

		
		System.out.println(dataInicio);

		//LocalDateTime dateTime = LocalDateTime.parse(dataInicio);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.CANADA_FRENCH); 
		LocalDateTime dateTime = LocalDateTime.parse(dataInicio, formatter);
		
		/*
		DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
		DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
		LocalDate date = LocalDate.parse("2018-04-10T04:00:00.000Z", inputFormatter);
		String formattedDate = outputFormatter.format(date);
		System.out.println(formattedDate); // prints 10-04-2018
	     */
		
		System.out.println(dateTime);
		
		RegistroLigacoesSearch filtro = new RegistroLigacoesSearch();
		filtro.setRamal(ramal);

		Specification<RegistroLigacoesEntity> spec = new RegistroLigacoesSpecification(filtro);

		return new ResponseEntity<>((Collection<RegistroLigacoesEntity>) repository.findAll(spec), HttpStatus.OK);
	}
	
	
	// Realiza Download da ligacao
	@RequestMapping(value = "/download/{nomeArquivo}", method = RequestMethod.GET)
	public HttpEntity<byte[]> download(@PathVariable("nomeArquivo") String nomeArquivo) throws IOException {

		System.out.println(nomeArquivo);
		byte[] arquivo = Files.readAllBytes(Paths.get("/home/luizgustavo/Imagens/" + nomeArquivo));

		HttpHeaders httpHeaders = new HttpHeaders();

		httpHeaders.add("Content-Disposition", "attachment;filename=\"download.png\"");

		HttpEntity<byte[]> entity = new HttpEntity<byte[]>(arquivo, httpHeaders);

		return entity;
	}
}
