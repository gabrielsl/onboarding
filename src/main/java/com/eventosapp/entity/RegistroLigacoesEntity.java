package com.eventosapp.entity;


import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cdr")
public class RegistroLigacoesEntity {
	
	@Id
	private String uniqueid;
	
	LocalDateTime calldate;
	
	private String clid;
	private String src;
	private String dst;
	private String dcontext;
	private String channel;
	private String dstchannel;
	private String lastapp;
	private String lastdata;
	private String disposition;
	private String accountcode;
	private String userfield;
	private int billsec;
	private int amaflags;
	private int duration;
	


	public String getClid() {
		return clid;
	}
	public LocalDateTime getCalldate() {
		return calldate;
	}
	public void setCalldate(LocalDateTime calldate) {
		this.calldate = calldate;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public void setClid(String clid) {
		this.clid = clid;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public String getDst() {
		return dst;
	}
	public void setDst(String dst) {
		this.dst = dst;
	}
	public String getDcontext() {
		return dcontext;
	}
	public void setDcontext(String dcontext) {
		this.dcontext = dcontext;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getDstchannel() {
		return dstchannel;
	}
	public void setDstchannel(String dstchannel) {
		this.dstchannel = dstchannel;
	}
	public String getLastapp() {
		return lastapp;
	}
	public void setLastapp(String lastapp) {
		this.lastapp = lastapp;
	}
	public String getLastdata() {
		return lastdata;
	}
	public void setLastdata(String lastdata) {
		this.lastdata = lastdata;
	}
	public String getDisposition() {
		return disposition;
	}
	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}
	public String getAccountcode() {
		return accountcode;
	}
	public void setAccountcode(String accountcode) {
		this.accountcode = accountcode;
	}
	public String getUniqueid() {
		return uniqueid;
	}
	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}
	public String getUserfield() {
		return userfield;
	}
	public void setUserfield(String userfield) {
		this.userfield = userfield;
	}
	public int getBillsec() {
		return billsec;
	}
	public void setBillsec(int billsec) {
		this.billsec = billsec;
	}
	public int getAmaflags() {
		return amaflags;
	}
	public void setAmaflags(int amaflags) {
		this.amaflags = amaflags;
	}
}
