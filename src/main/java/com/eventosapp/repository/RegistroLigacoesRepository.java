package com.eventosapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.eventosapp.entity.RegistroLigacoesEntity;
import com.eventosapp.models.Agregados;

public interface RegistroLigacoesRepository extends JpaRepository<RegistroLigacoesEntity, String>, JpaSpecificationExecutor<RegistroLigacoesEntity>{

		
		@Override
		@Query("select u from RegistroLigacoesEntity u where u.userfield is not null")
		//Ligações que não foram gravadas, não devem ser retornadas.
		public List<RegistroLigacoesEntity> findAll();
		
		@Query("select new com.eventosapp.models.Agregados(count(u) as count, count(u) as sum) from RegistroLigacoesEntity u where u.userfield is not null")
	    public Agregados retorno();
		
		public List<RegistroLigacoesEntity> findByUniqueid(String uniqueid);
}
