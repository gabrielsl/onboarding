var appOnboarding= angular.module("appOnboarding", [ 'ngRoute' ]);

appOnboarding.config(function($routeProvider) {

	$routeProvider.when("/registroLigacoes", {
		templateUrl : 'view/formLigacoes.html',
		controller : 'formLigacoesController'
	}).otherwise({
		rediretTo : '/'
	});
});