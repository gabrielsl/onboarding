appOnboarding.controller("formLigacoesController", function($scope, $http) {

	$scope.ligacoes = [];

	$scope.buscarLigacoes = function() {
		$http({
			method : 'GET',
			url : 'http://localhost:8080/ligacoes/ramal',
			params: {
					ramal: $scope.ramal,
					dst: $scope.dst,
					dataInicio: $scope.dataInicio
					}
		}).then(function successCallback(response) {
			console.log($scope.dataInicio);
			$scope.ligacoes = response.data;
			console.log($scope.ligacoes);

			$(document).ready(function() {
    			$('#form-ligacoes').DataTable( {
         				"language": {
                							"url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            		}
    			} );
			} );

		}, function errorCallback(response) {
			console.log(response.status)
		});
	}


	/*bkp*/
	$scope.buscarLigacoesBKP = function() {
		$http({
			method : 'GET',
			url : 'http://localhost:8080/filtroRegistroLigacoes/{{dst}}'
		}).then(function successCallback(response) {
			console.log(dst)
			$scope.ligacoes = response.data;
			console.log($scope.ligacoes);
		}, function errorCallback(response) {
			console.log(response.status)
		});
	}

});
