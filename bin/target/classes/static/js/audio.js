// create angular app
var ngApp = angular.module('ngApp', []);

ngApp.directive('pkAudio', function () {
    return {
        restrict: 'A',
        scope: {
            pkAudio: '='
        },
        link: function (scope, element, attrs, ngModelCtrl) {

            var removeBehaviorsRestrictions = function() {
                element.load();
                window.removeEventListener('keydown', removeBehaviorsRestrictions);
                window.removeEventListener('mousedown', removeBehaviorsRestrictions);
                window.removeEventListener('touchstart', removeBehaviorsRestrictions);
            };
            window.addEventListener('keydown', removeBehaviorsRestrictions);
            window.addEventListener('mousedown', removeBehaviorsRestrictions);
            window.addEventListener('touchstart', removeBehaviorsRestrictions);

            scope.pkAudio = element[0];
        }
    };
});

// create angular controller
ngApp.controller('mainController',['$scope', '$interval',  function($scope, $interval) {
  $scope.sounds = {};
  
  $scope.play = function(){
  };
  
  $interval(function(){
    $scope.sounds.sound.play();
  },1500);

}]);